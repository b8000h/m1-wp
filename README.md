https://fishpig.co.uk/magento/wordpress-integration/

NOTE: 如果 Magento 站默认是 https:// 则注意 WordPress 的 Site address 也须 https://
默认会往 WordPress 的 Widgets > Main Sidebar > Meta 放些链接，直接将 Meta 拖到 Inactive Widgets 下即可

【TOC]

## Installing WordPress

Before you can integrate WordPress into your Magento eCommerce website, you must first install WordPress. It's really easy to do yourself and if your hosting company has a 1-click WordPress installer, I would recommend not using this and doing it yourself. This will give you finer control over things and allow you to follow this tutorial.

Before starting this process, ensure that you have FTP access to your Magento website and are comfortable uploading files.

Downloading WordPress
To download WordPress, head on over to https://wordpress.org/ and download the latest version to your computer. This will provide you with a ZIP file containing all of the WordPress application.

Uploading WordPress to your Server
To upload WordPress to your server, first double click the ZIP file and extract everything. This should produce a folder called 'wordpress' that contains all of the WordPress files and folders. Rename this folder to 'wp' and upload it to your server in the root of your Magento website. The root of your website is the base folder that contains the index.php file, app directory, skin directory and many more files and folders. Once this has uploaded fully you can test it by loading up your web browser and typing in your Magento URL and at the end, adding '/wp/'. As an example, if your Magento URL was http://www.example.com/, you would go to http://www.example.com/wp/ in your browser.

If everything has uploaded correctly you should see a WordPress installation page.

Why 'wp' and not 'blog'?
Before we actually install WordPress, I want to explain why we uploaded WordPress to the /wp/ directory, rather than something useful and usable, such as /blog/. When you request a page from Magento, almost all requests are sent through Magento's index.php file, which then routes the request to the correct module in Magento. This all happens transparently and the user never knows that this is happening. Of the few requests that aren't routed to Magento, one is requests that point to a file or folder that actually exists on the server. Because of this, if we installed WordPress at /blog/, any requests to /blog/ would go straight to WordPress, which wouldn't be integrated into Magento. To get around this, we install WordPress at /wp/ (this can be anything other than where you actually want your integrated blog to be) and then later on, we will configure the Magento WordPress Integration extension to display the integrated blog at /blog/. With this setup, requests to /blog/ will be routed through Magento's index.php file (as the /blog/ directory doesn't exist) and then passed to the Magento WordPress Integration extension to display the integrated blog.

Installing WordPress
Follow the steps outlined on the screen to install WordPress. When you get to the database section, either create a new database and enter those details or enter your Magento database details (recommended but not required). If you are unsure of your Magento database details, locate the app/etc/local.xml file via FTP and you will find the Magento database details here.

WordPress will use 'wp_' as a table prefix by default. This is really useful as it makes it easy to separate your Magento database table from your WordPress database tables if you are sharing a database between the two applications. While it's okay to change it (eg. wp_blog_'), ensure that you do enter something in this field.

Summary
You should now have a fully functioning WordPress installation on your Magento server. This installation of WordPress isn't integrated into Magento yet, but it's ready to be integrated. When you're ready, check out our installation guide for Magento WordPress Integration.


---


## Installing Magento WordPress Integration

If you haven't already installed WordPress, check out our guide to installing WordPress for Magento WordPress Integration.

With WordPress installed, let's get started installing Magento WordPress Integration.

Installing the Extension
The extension is completely free and can be installed like any other Magento extension. Before installing though, ensure that the Magento cache is fully disabled and that the Magento compiler (if it's enabled), is disabled. You can refresh and recompile these after the installation.

For more details on the installing the Magento WordPress Integration extension, see our guide on installing Magento extensions.

Configuring Magento WordPress Integration
With the extension installed, let's configure it so that it connects to WordPress and integrates your WordPress blog into your Magento theme. Login to your Magento Admin and select WordPress > Settings from the top navigation. Once the configuration page loads, there are 2 sections that we need to look at: Database and Integration. Before configuring these sections, I just want to point out that if you have any error/warning messages then that's fine as the configuration changes we are about to make will fix those.

Database
To connect to your WordPress database, the Magento WordPress Integration extension will need to know the connection details. If you installed WordPress in the same database as Magento, this easy and you can simply select 'Yes' for the 'Do Magento and WordPress share a database?' option and enter your table prefix. If WordPress is installed in a different database or if you just aren't sure, you can find your WordPress details in the wp-config.php file that lives inside your WordPress installation. If you installed WordPress at /wp/, this file will be at /wp/wp-config.php. If you can't find this file, ensure that you have installed WordPress.

Once you have your WordPress database details entered correctly, press the 'Save Config' button. After the page loads, some of the error/warning messages that were displayed should now be gone.

Integration
There are 3 key fields in this section that we need to go through to get Magento properly integrated: Integrate Theme, Home URL and Path.

Integrate Theme
For 99.9% of users, this should be set to 'Yes'. This means that you want your WordPress blog to be integrated into your Magento theme. If you set this to 'No', WordPress will not be integrated into Magento and the extension will simply connect to your WordPress database and do nothing else. To simplify things, just set this to 'Yes'.

Home URL
In this field, what ever you entered will be added to your Magento base URL and become the URL that your integrated WordPress blog is visible on. In this tutorial, I am going to setup my WordPress blog to be integrated at /blog/ and will use this throughout the tutorial. If you want to set your integrated blog up at a different location (eg. /news/ or /articles/) then simply substitue where ever I write 'blog' for what ever you choose to use.

When entering the value in this field, you do not need to enter the trailing or initial '/' character. Using the blog example, you would simply enter 'blog'.

Path
Finally, the Path field should contain the path to your WordPress installation. This can be the absolute path (starts with a '/' character) or a relative path. If you have installed WordPress in a sub-directory, you can simply enter the name of that sub-directory into this field. As an example, if you installed WordPress in a sub-directory called 'wp', simply enter 'wp' in this field.

Click the 'Save Config' button and wait for the page to reload. You will most likely still have some yellow warning messages at the top of the page (don't worry, we'll take care of those in a minute) but your WordPress blog should now be integrated into Magento. To test this, go to the URL you setup to be your integrated blog URL (remember, this is your Magento base URL and what ever value you entered in the Home URL field). Using the 'blog' example, this would be http://www.example.com/blog/.

Configuring WordPress for Magento Integration
Now let's fix those remaining warnings that sit at the top of the configuration section of the extension in the Magento Admin. To do this, we will need to make some configuration changes in WordPress. To do this, first login to your WordPress Admin. To do this, add 'wp-admin' to what ever your WordPress URL is (eg. http://www.example.com/wp/wp-admin/) and login with the username and password you created while installing WordPress.

WordPress URLs
Just like Magento, WordPress stores the URLs that it is configured to use in it's database. These URLs live in the 'options' database table (if your table prefix is 'wp_', this table would be 'wp_options') and have the keys 'siteurl' and 'home'. To edit these, select Settings > General from the WordPress Admin navigation. Let's explain what each URL does and what it should be set as for a proper integration.

WordPress Address URL (siteurl)
On the Settings > General page of the WordPress Admin, you should see two input fields that contain URLs. The first URL is labelled the 'WordPress Address URL' and is stored in the 'options' table with the key 'siteurl'. This URL is the URL that WordPress is installed on and should not be changed. If Magento is installed at http://www.example.com/ and WordPress is installed in a sub-directory called 'wp', this URL should be 'http://www.example.com/wp'. Ensure that if Magento is installed with 'www.' in it's URL that this URL also contains 'www.'. If Magento does not have 'www.' in it's URL, ensure this URL also does not contain 'www.'.

Site Address URL (home)
This URL is the second URL on the Settings > General page and contains the URL that the frontend of your blog be will available on. In a normal WordPress installation, this URL would match the WordPress Address URL (the first URL on the Settings > Page), however with an integrated WordPress installation, this URL should be your Magento base URL and then what ever you entered in the Home URL field while configuring Magento WordPress Integration. Using the example I used earlier, if Magento is installed on http://www.example.com/ and your Home URL value in the extension's configuration was 'blog', this URL should be 'http://www.example.com/blog'. As with the WordPress Address URL, ensure that if Magento has a 'www.' in it's URL, this URL also has a 'www.' and if Magento doesn't have a 'www.', this URL also doesn't have a 'www.'.

WordPress Theme
When WordPress is integrated into Magento, your Magento theme is used to display your blog, which means your WordPress theme isn't used for much. While developing and testing Magento WordPress Integration, I use the Twenty Twelve theme for WordPress. While it doesn't affect the frontend of your blog (remember, the integration uses your Magento theme to display the blog), it does have a small impact on a few integration features so it is highly recommended that you install the Twenty Twelve theme. To do this, select Appearance > Themes > Add New from your WordPress Admin and search for and install the Twenty Twelve theme.

Permalinks
WordPress uses ugly links for blog posts by default but this can be changed really easily and should be changed. To do this, select Settings > Permalinks from the WordPress Admin and select the Permalink structure that is right for you.

Troubleshooting
If you go to your integrated blog URL and you don't see the blog, check whether you have a CMS Page or Category setup to use 'blog' (or whatever you tried to display your blog on) as it's URL Key. If it does, delete it and the blog should display correctly.

If the blog still isn't displaying, check whether you have the AheadWorks Blog (AW_Blog) module installed. AheadWorks have created this extension using 'blog' as the internal frontName for their extension, which means that when installed, it's not possible to integrate WordPress on /blog/. To resolve this, either delete the AW_Blog extension or integrate WordPress at a different location.
